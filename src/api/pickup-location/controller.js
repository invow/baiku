import { success, notFound, authorOrAdmin } from '../../services/response/'
import { PickupLocation } from '.'

export const create = ({ user, bodymen: { body } }, res, next) =>
  PickupLocation.create({ ...body, user })
    .then((pickupLocation) => pickupLocation.view(true))
    .then(success(res, 201))
    .catch(next)

export const index = ({ querymen: { query, select, cursor } }, res, next) =>
  PickupLocation.find(query, select, cursor)
    .populate('user')
    .then((pickupLocations) => pickupLocations.map((pickupLocation) => pickupLocation.view()))
    .then(success(res))
    .catch(next)

export const show = ({ params }, res, next) =>
  PickupLocation.findById(params.id)
    .populate('user')
    .then(notFound(res))
    .then((pickupLocation) => pickupLocation ? pickupLocation.view() : null)
    .then(success(res))
    .catch(next)

export const update = ({ user, bodymen: { body }, params }, res, next) =>
  PickupLocation.findById(params.id)
    .populate('user')
    .then(notFound(res))
    .then(authorOrAdmin(res, user, 'user'))
    .then((pickupLocation) => pickupLocation ? Object.assign(pickupLocation, body).save() : null)
    .then((pickupLocation) => pickupLocation ? pickupLocation.view(true) : null)
    .then(success(res))
    .catch(next)

export const destroy = ({ user, params }, res, next) =>
  PickupLocation.findById(params.id)
    .then(notFound(res))
    .then(authorOrAdmin(res, user, 'user'))
    .then((pickupLocation) => pickupLocation ? pickupLocation.remove() : null)
    .then(success(res, 204))
    .catch(next)
