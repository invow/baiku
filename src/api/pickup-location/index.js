import { Router } from 'express'
import { middleware as query } from 'querymen'
import { middleware as body } from 'bodymen'
import { token } from '../../services/passport'
import { create, index, show, update, destroy } from './controller'
import { schema } from './model'
export PickupLocation, { schema } from './model'

const router = new Router()
const { lat, long } = schema.tree

/**
 * @api {post} /pickup-locations Create pickup location
 * @apiName CreatePickupLocation
 * @apiGroup PickupLocation
 * @apiPermission user
 * @apiParam {String} access_token user access token.
 * @apiParam lat Pickup location's lat.
 * @apiParam long Pickup location's long.
 * @apiSuccess {Object} pickupLocation Pickup location's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Pickup location not found.
 * @apiError 401 user access only.
 */
router.post('/',
  token({ required: true }),
  body({ lat, long }),
  create)

/**
 * @api {get} /pickup-locations Retrieve pickup locations
 * @apiName RetrievePickupLocations
 * @apiGroup PickupLocation
 * @apiPermission user
 * @apiParam {String} access_token user access token.
 * @apiUse listParams
 * @apiSuccess {Object[]} pickupLocations List of pickup locations.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 401 user access only.
 */
router.get('/',
  token({ required: true }),
  query(),
  index)

/**
 * @api {get} /pickup-locations/:id Retrieve pickup location
 * @apiName RetrievePickupLocation
 * @apiGroup PickupLocation
 * @apiPermission user
 * @apiParam {String} access_token user access token.
 * @apiSuccess {Object} pickupLocation Pickup location's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Pickup location not found.
 * @apiError 401 user access only.
 */
router.get('/:id',
  token({ required: true }),
  show)

/**
 * @api {put} /pickup-locations/:id Update pickup location
 * @apiName UpdatePickupLocation
 * @apiGroup PickupLocation
 * @apiPermission user
 * @apiParam {String} access_token user access token.
 * @apiParam lat Pickup location's lat.
 * @apiParam long Pickup location's long.
 * @apiSuccess {Object} pickupLocation Pickup location's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Pickup location not found.
 * @apiError 401 user access only.
 */
router.put('/:id',
  token({ required: true }),
  body({ lat, long }),
  update)

/**
 * @api {delete} /pickup-locations/:id Delete pickup location
 * @apiName DeletePickupLocation
 * @apiGroup PickupLocation
 * @apiPermission user
 * @apiParam {String} access_token user access token.
 * @apiSuccess (Success 204) 204 No Content.
 * @apiError 404 Pickup location not found.
 * @apiError 401 user access only.
 */
router.delete('/:id',
  token({ required: true }),
  destroy)

export default router
