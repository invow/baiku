import { success, notFound, authorOrAdmin } from '../../services/response/'
import { DropOffLocation } from '.'
import { Bicycle } from '../bicycle'
import { PickupLocation } from '../pickup-location'
import Distance from 'geo-distance'

export const create = ({ user, bodymen: { body } }, res, next) =>
  DropOffLocation.create({ ...body, user })
    .then((dropOffLocation) => dropOffLocation.view(true))
    .then(success(res, 201))
    .catch(next)

export const index = ({ params, querymen: { query, select, cursor } }, res, next) => {
  Bicycle.findOne({ sixCharId: params.sixCharId })
    .then((bicycle) => {
      PickupLocation.findById(bicycle.pickupLocation)
        .then((pickupLocation) => {
          DropOffLocation.find(query, select, cursor)
            .then((dropOffLocations) => dropOffLocations.map((dropOffLocation) => {
              const point1 = {
                lat: pickupLocation.lat,
                lon: pickupLocation.long
              }
              const point2 = {
                lat: dropOffLocation.lat,
                lon: dropOffLocation.long
              }

              var calculateDistance = Distance.between(point1, point2)
              console.log('' + calculateDistance.human_readable())
              if (calculateDistance < Distance('10 km')) {
                res.json({
                  userCoordinates: {
                    lat: dropOffLocation.lat,
                    lng: dropOffLocation.long
                  }
                })
              } else {
                res.json({
                  message: 'We could not find nothing near to you.'
                })
              }
            }))
        })
    })
}

export const show = ({ params }, res, next) =>
  DropOffLocation.findById(params.id)
    .populate('user')
    .then(notFound(res))
    .then((dropOffLocation) => dropOffLocation ? dropOffLocation.view() : null)
    .then(success(res))
    .catch(next)

export const update = ({ user, bodymen: { body }, params }, res, next) =>
  DropOffLocation.findById(params.id)
    .populate('user')
    .then(notFound(res))
    .then(authorOrAdmin(res, user, 'user'))
    .then((dropOffLocation) => dropOffLocation ? Object.assign(dropOffLocation, body).save() : null)
    .then((dropOffLocation) => dropOffLocation ? dropOffLocation.view(true) : null)
    .then(success(res))
    .catch(next)

export const destroy = ({ user, params }, res, next) =>
  DropOffLocation.findById(params.id)
    .then(notFound(res))
    .then(authorOrAdmin(res, user, 'user'))
    .then((dropOffLocation) => dropOffLocation ? dropOffLocation.remove() : null)
    .then(success(res, 204))
    .catch(next)
