import { DropOffLocation } from '.'
import { User } from '../user'

let user, dropOffLocation

beforeEach(async () => {
  user = await User.create({ email: 'a@a.com', password: '123456' })
  dropOffLocation = await DropOffLocation.create({ user, lat: 'test', long: 'test' })
})

describe('view', () => {
  it('returns simple view', () => {
    const view = dropOffLocation.view()
    expect(typeof view).toBe('object')
    expect(view.id).toBe(dropOffLocation.id)
    expect(typeof view.user).toBe('object')
    expect(view.user.id).toBe(user.id)
    expect(view.lat).toBe(dropOffLocation.lat)
    expect(view.long).toBe(dropOffLocation.long)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })

  it('returns full view', () => {
    const view = dropOffLocation.view(true)
    expect(typeof view).toBe('object')
    expect(view.id).toBe(dropOffLocation.id)
    expect(typeof view.user).toBe('object')
    expect(view.user.id).toBe(user.id)
    expect(view.lat).toBe(dropOffLocation.lat)
    expect(view.long).toBe(dropOffLocation.long)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })
})
