import { Router } from 'express'
import { middleware as query } from 'querymen'
import { middleware as body } from 'bodymen'
import { token } from '../../services/passport'
import { create, index, show, update, destroy } from './controller'
import { schema } from './model'
export DropOffLocation, { schema } from './model'

const router = new Router()
const { lat, long } = schema.tree

/**
 * @api {post} /drop-off-locations Create drop off location
 * @apiName CreateDropOffLocation
 * @apiGroup DropOffLocation
 * @apiPermission user
 * @apiParam {String} access_token user access token.
 * @apiParam lat Drop off location's lat.
 * @apiParam long Drop off location's long.
 * @apiSuccess {Object} dropOffLocation Drop off location's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Drop off location not found.
 * @apiError 401 user access only.
 */
router.post('/',
  token({ required: true }),
  body({ lat, long }),
  create)

/**
 * @api {get} /drop-off-locations Retrieve drop off locations
 * @apiName RetrieveDropOffLocations
 * @apiGroup DropOffLocation
 * @apiPermission user
 * @apiParam {String} access_token user access token.
 * @apiUse listParams
 * @apiSuccess {Object[]} dropOffLocations List of drop off locations.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 401 user access only.
 */
router.get('/:sixCharId',
  token({ required: true }),
  query(),
  index)

/**
 * @api {get} /drop-off-locations/:id Retrieve drop off location
 * @apiName RetrieveDropOffLocation
 * @apiGroup DropOffLocation
 * @apiPermission user
 * @apiParam {String} access_token user access token.
 * @apiSuccess {Object} dropOffLocation Drop off location's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Drop off location not found.
 * @apiError 401 user access only.
 */
router.get('/:id',
  token({ required: true }),
  show)

/**
 * @api {put} /drop-off-locations/:id Update drop off location
 * @apiName UpdateDropOffLocation
 * @apiGroup DropOffLocation
 * @apiPermission user
 * @apiParam {String} access_token user access token.
 * @apiParam lat Drop off location's lat.
 * @apiParam long Drop off location's long.
 * @apiSuccess {Object} dropOffLocation Drop off location's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Drop off location not found.
 * @apiError 401 user access only.
 */
router.put('/:id',
  token({ required: true }),
  body({ lat, long }),
  update)

/**
 * @api {delete} /drop-off-locations/:id Delete drop off location
 * @apiName DeleteDropOffLocation
 * @apiGroup DropOffLocation
 * @apiPermission user
 * @apiParam {String} access_token user access token.
 * @apiSuccess (Success 204) 204 No Content.
 * @apiError 404 Drop off location not found.
 * @apiError 401 user access only.
 */
router.delete('/:id',
  token({ required: true }),
  destroy)

export default router
