import { Reservation } from '.'
import { User } from '../user'

let user, reservation

beforeEach(async () => {
  user = await User.create({ email: 'a@a.com', password: '123456' })
  reservation = await Reservation.create({ user, pickupLocation: 'test', status: 'test' })
})

describe('view', () => {
  it('returns simple view', () => {
    const view = reservation.view()
    expect(typeof view).toBe('object')
    expect(view.id).toBe(reservation.id)
    expect(typeof view.user).toBe('object')
    expect(view.user.id).toBe(user.id)
    expect(view.pickupLocation).toBe(reservation.pickupLocation)
    expect(view.status).toBe(reservation.status)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })

  it('returns full view', () => {
    const view = reservation.view(true)
    expect(typeof view).toBe('object')
    expect(view.id).toBe(reservation.id)
    expect(typeof view.user).toBe('object')
    expect(view.user.id).toBe(user.id)
    expect(view.pickupLocation).toBe(reservation.pickupLocation)
    expect(view.status).toBe(reservation.status)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })
})
