import mongoose, { Schema } from 'mongoose'

const reservationSchema = new Schema({
  user: {
    type: Schema.ObjectId,
    ref: 'User',
    required: true
  },
  pickupLocation: {
    type: String
  },
  status: {
    type: String
  },
  bicycle: {
    type: String
  }
}, {
  timestamps: true,
  toJSON: {
    virtuals: true,
    transform: (obj, ret) => { delete ret._id }
  }
})

reservationSchema.methods = {
  view (full) {
    const view = {
      // simple view
      id: this.id,
      user: this.user.view(full),
      pickupLocation: this.pickupLocation,
      status: this.status,
      bicycle: this.bicycle,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt
    }

    return full ? {
      ...view
      // add properties for a full view
    } : view
  }
}

const model = mongoose.model('Reservation', reservationSchema)

export const schema = model.schema
export default model
