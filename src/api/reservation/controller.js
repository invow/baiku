import { success, notFound, authorOrAdmin } from '../../services/response/'
import { Reservation } from '.'

export const create = ({ user, bodymen: { body } }, res, next) =>
  Reservation.create({ ...body, user })
    .then((reservation) => reservation.view(true))
    .then(success(res, 201))
    .catch(next)

export const index = ({ querymen: { query, select, cursor } }, res, next) =>
  Reservation.find(query, select, cursor)
    .populate('user')
    .then((reservations) => reservations.map((reservation) => reservation.view()))
    .then(success(res))
    .catch(next)

export const show = ({ params }, res, next) =>
  Reservation.findById(params.id)
    .populate('user')
    .then(notFound(res))
    .then((reservation) => reservation ? reservation.view() : null)
    .then(success(res))
    .catch(next)

export const update = ({ user, bodymen: { body }, params }, res, next) =>
  Reservation.findById(params.id)
    .populate('user')
    .then(notFound(res))
    .then(authorOrAdmin(res, user, 'user'))
    .then((reservation) => reservation ? Object.assign(reservation, body).save() : null)
    .then((reservation) => reservation ? reservation.view(true) : null)
    .then(success(res))
    .catch(next)

export const destroy = ({ user, params }, res, next) =>
  Reservation.findById(params.id)
    .then(notFound(res))
    .then(authorOrAdmin(res, user, 'user'))
    .then((reservation) => reservation ? reservation.remove() : null)
    .then(success(res, 204))
    .catch(next)
