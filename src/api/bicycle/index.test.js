import request from 'supertest'
import { apiRoot } from '../../config'
import { signSync } from '../../services/jwt'
import express from '../../services/express'
import { User } from '../user'
import routes, { Bicycle } from '.'

const app = () => express(apiRoot, routes)

let userSession, adminSession, bicycle

beforeEach(async () => {
  const user = await User.create({ email: 'a@a.com', password: '123456' })
  const admin = await User.create({ email: 'c@c.com', password: '123456', role: 'admin' })
  userSession = signSync(user.id)
  adminSession = signSync(admin.id)
  bicycle = await Bicycle.create({})
})

test('POST /bicycles 201 (admin)', async () => {
  const { status, body } = await request(app())
    .post(`${apiRoot}`)
    .send({ access_token: adminSession, sixCharId: 'test', pickupLocation: 'test', status: 'test' })
  expect(status).toBe(201)
  expect(typeof body).toEqual('object')
  expect(body.sixCharId).toEqual('test')
  expect(body.pickupLocation).toEqual('test')
  expect(body.status).toEqual('test')
})

test('POST /bicycles 401 (user)', async () => {
  const { status } = await request(app())
    .post(`${apiRoot}`)
    .send({ access_token: userSession })
  expect(status).toBe(401)
})

test('POST /bicycles 401', async () => {
  const { status } = await request(app())
    .post(`${apiRoot}`)
  expect(status).toBe(401)
})

test('GET /bicycles 200', async () => {
  const { status, body } = await request(app())
    .get(`${apiRoot}`)
  expect(status).toBe(200)
  expect(Array.isArray(body)).toBe(true)
})

test('GET /bicycles/:id 200', async () => {
  const { status, body } = await request(app())
    .get(`${apiRoot}/${bicycle.id}`)
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(bicycle.id)
})

test('GET /bicycles/:id 404', async () => {
  const { status } = await request(app())
    .get(apiRoot + '/123456789098765432123456')
  expect(status).toBe(404)
})

test('PUT /bicycles/:id 200 (admin)', async () => {
  const { status, body } = await request(app())
    .put(`${apiRoot}/${bicycle.id}`)
    .send({ access_token: adminSession, sixCharId: 'test', pickupLocation: 'test', status: 'test' })
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(bicycle.id)
  expect(body.sixCharId).toEqual('test')
  expect(body.pickupLocation).toEqual('test')
  expect(body.status).toEqual('test')
})

test('PUT /bicycles/:id 401 (user)', async () => {
  const { status } = await request(app())
    .put(`${apiRoot}/${bicycle.id}`)
    .send({ access_token: userSession })
  expect(status).toBe(401)
})

test('PUT /bicycles/:id 401', async () => {
  const { status } = await request(app())
    .put(`${apiRoot}/${bicycle.id}`)
  expect(status).toBe(401)
})

test('PUT /bicycles/:id 404 (admin)', async () => {
  const { status } = await request(app())
    .put(apiRoot + '/123456789098765432123456')
    .send({ access_token: adminSession, sixCharId: 'test', pickupLocation: 'test', status: 'test' })
  expect(status).toBe(404)
})

test('DELETE /bicycles/:id 204 (admin)', async () => {
  const { status } = await request(app())
    .delete(`${apiRoot}/${bicycle.id}`)
    .query({ access_token: adminSession })
  expect(status).toBe(204)
})

test('DELETE /bicycles/:id 401 (user)', async () => {
  const { status } = await request(app())
    .delete(`${apiRoot}/${bicycle.id}`)
    .query({ access_token: userSession })
  expect(status).toBe(401)
})

test('DELETE /bicycles/:id 401', async () => {
  const { status } = await request(app())
    .delete(`${apiRoot}/${bicycle.id}`)
  expect(status).toBe(401)
})

test('DELETE /bicycles/:id 404 (admin)', async () => {
  const { status } = await request(app())
    .delete(apiRoot + '/123456789098765432123456')
    .query({ access_token: adminSession })
  expect(status).toBe(404)
})
