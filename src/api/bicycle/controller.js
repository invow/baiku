import { success, notFound } from '../../services/response/'
import { Bicycle } from '.'
import { PickupLocation } from '../pickup-location'
import { DropOffLocation } from '../drop-off-location'

export const create = ({ bodymen: { body } }, res, next) => {
  body.status = 'locked'
  Bicycle.create(body)
    .then((bicycle) => bicycle.view(true))
    .then(success(res, 201))
    .catch(next)
}

export const createPickup = ({ user, params, bodymen: { body } }, res, next) => {
  Bicycle.findOne({ sixCharId: params.sixCharId })
    .then((bicycle) => {
      bicycle.status = 'approved'
      PickupLocation.create({ ...body, user })
        .then((pickupLocation) => {
          bicycle.pickupLocation = pickupLocation.id
          bicycle.save()
          res.json({
            userCoordinates: {
              lat: pickupLocation.lat,
              lng: pickupLocation.long
            }
          })
        })
    })
}

export const getPickupLocation = ({ params }, res, next) =>
  Bicycle.find({ pickupLocation: params.locationId })
    .then((bicycles) => bicycles.map((bicycle) => bicycle.view()))
    .then(success(res))
    .catch(next)

export const index = ({ querymen: { query, select, cursor } }, res, next) =>
  Bicycle.find(query, select, cursor)
    .then((bicycles) => bicycles.map((bicycle) => bicycle.view()))
    .then(success(res))
    .catch(next)

export const showDropoff = ({ querymen: { query, select, cursor } }, res, next) =>
  DropOffLocation.find(query, select, cursor)
    .populate('user')
    .then((dropOffLocations) => dropOffLocations.map((dropOffLocation) => dropOffLocation.view()))
    .then(success(res))
    .catch(next)

export const show = ({ params }, res, next) =>
  Bicycle.findById(params.id)
    .then(notFound(res))
    .then((bicycle) => bicycle ? bicycle.view() : null)
    .then(success(res))
    .catch(next)

export const bySixCharId = ({ params }, res, next) =>
  Bicycle.findOne({ sixCharId: params.sixCharId })
    .then(notFound(res))
    .then((bicycle) => (bicycle ? bicycle.view() : null))
    .then(success(res))
    .catch(next)

export const update = ({ bodymen: { body }, params }, res, next) =>
  Bicycle.findById(params.id)
    .then(notFound(res))
    .then((bicycle) => bicycle ? Object.assign(bicycle, body).save() : null)
    .then((bicycle) => bicycle ? bicycle.view(true) : null)
    .then(success(res))
    .catch(next)

export const destroy = ({ params }, res, next) =>
  Bicycle.findById(params.id)
    .then(notFound(res))
    .then((bicycle) => bicycle ? bicycle.remove() : null)
    .then(success(res, 204))
    .catch(next)
