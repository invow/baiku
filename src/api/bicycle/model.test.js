import { Bicycle } from '.'

let bicycle

beforeEach(async () => {
  bicycle = await Bicycle.create({ sixCharId: 'test', pickupLocation: 'test', status: 'test' })
})

describe('view', () => {
  it('returns simple view', () => {
    const view = bicycle.view()
    expect(typeof view).toBe('object')
    expect(view.id).toBe(bicycle.id)
    expect(view.sixCharId).toBe(bicycle.sixCharId)
    expect(view.pickupLocation).toBe(bicycle.pickupLocation)
    expect(view.status).toBe(bicycle.status)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })

  it('returns full view', () => {
    const view = bicycle.view(true)
    expect(typeof view).toBe('object')
    expect(view.id).toBe(bicycle.id)
    expect(view.sixCharId).toBe(bicycle.sixCharId)
    expect(view.pickupLocation).toBe(bicycle.pickupLocation)
    expect(view.status).toBe(bicycle.status)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })
})
