import mongoose, { Schema } from 'mongoose'

const bicycleSchema = new Schema({
  sixCharId: {
    type: Number,
    required: true
  },
  pickupLocation: {
    type: String
  },
  type: {
    type: String,
    enum: ['touring', 'sport'],
    required: true
  },
  status: {
    type: String,
    enum: ['locked', 'approved']
  }

}, {
  timestamps: true,
  toJSON: {
    virtuals: true,
    transform: (obj, ret) => { delete ret._id }
  }
})

bicycleSchema.methods = {
  view (full) {
    const view = {
      // simple view
      id: this.id,
      sixCharId: this.sixCharId,
      pickupLocation: this.pickupLocation,
      status: this.status,
      type: this.type,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt
    }

    return full ? {
      ...view
      // add properties for a full view
    } : view
  }
}

const model = mongoose.model('Bicycle', bicycleSchema)

export const schema = model.schema
export default model
