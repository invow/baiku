import { Router } from 'express'
import { middleware as query } from 'querymen'
import { middleware as body } from 'bodymen'
import { token } from '../../services/passport'
import { create, index, show, getPickupLocation, createPickup, showDropoff, update, destroy, bySixCharId } from './controller'
import { schema } from './model'
import { schema as pickupSchema } from '../pickup-location/model'
export Bicycle, { schema } from './model'
export { pickupSchema } from '../pickup-location/model'
const router = new Router()
const { sixCharId, pickupLocation, status, type } = schema.tree
const { lat, lng } = pickupSchema.tree

/**
 * @api {post} /bicycles Create bicycle
 * @apiName CreateBicycle
 * @apiGroup Bicycle
 * @apiPermission admin
 * @apiParam {String} access_token admin access token.
 * @apiParam sixCharId Bicycle's sixCharId.
 * @apiParam pickupLocation Bicycle's pickupLocation.
 * @apiParam status Bicycle's status.
 * @apiSuccess {Object} bicycle Bicycle's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Bicycle not found.
 * @apiError 401 admin access only.
 */
router.post('/',
  token({ required: true }),
  body({ sixCharId, pickupLocation, status, type }),
  create)

/**
* @api {post} /bicycles/:sixCharId/pickup Create pickup
* @apiName CreatePickup
* @apiGroup Bicycle
* @apiParam {String} access_token admin access token.
* @apiParam sixCharId Bicycle's sixCharId.
* @apiSuccess {Object} bicycle User coordenates data.
* @apiError {Object} 400 Some parameters may contain invalid values.
* @apiError 404 Bicycle not found.
*/
router.post('/:sixCharId/pickup',
  token({ required: true }),
  body({ lat, lng }),
  createPickup)

/**
* @api {post} /bicycles/:sixCharId/dropoff Show dropoff
* @apiName ShowDropoff
* @apiGroup Bicycle
* @apiParam {String} access_token admin access token.
* @apiParam sixCharId Bicycle's sixCharId.
* @apiSuccess {Object} bicycle User coordenates data.
* @apiError {Object} 400 Some parameters may contain invalid values.
* @apiError 404 Bicycle not found.
*/
router.post('/:sixCharId/dropoff',
  token({ required: true }),
  showDropoff)

/**
 * @api {get} /bicycles Retrieve bicycles
 * @apiName RetrieveBicycles
 * @apiGroup Bicycle
 * @apiUse listParams
 * @apiSuccess {Object[]} bicycles List of bicycles.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 */
router.get('/',
  query(),
  index)

/**
 * @api {get} /bicycles/:id Retrieve bicycle
 * @apiName RetrieveBicycle
 * @apiGroup Bicycle
 * @apiSuccess {Object} bicycle Bicycle's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Bicycle not found.
 */
router.get('/:id',
  show)

/**
 * @api {get} /bicycles/:sixCharId Retrieve bicycle
 * @apiName RetrieveBicycle
 * @apiGroup Bicycle
 * @apiSuccess {Object} bicycle Bicycle's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Bicycle not found.
 */
router.get('/:sixCharId',
  bySixCharId)

/**
 * @api {get} /bicycles/:sixCharId/dropoff Retrieve dropoff locations
 * @apiName RetrieveDropoffLocations
 * @apiGroup Bicycle
 * @apiUse listParams
 * @apiSuccess {Object[]} bicycles List of dropoff locations.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 */
router.get('/:sixCharId/dropoff',
  query(),
  showDropoff)

/**
 * @api {get} /bicycles/pickup-location/:locationId Retrieve bicycle
 * @apiName RetrievePickupLocation
 * @apiGroup Bicycle
 * @apiSuccess {Object} bicycle PickupLocation's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 PickupLocation not found.
 */
router.get('/pickup-location/:locationId',
  getPickupLocation)

/**
 * @api {put} /bicycles/:id Update bicycle
 * @apiName UpdateBicycle
 * @apiGroup Bicycle
 * @apiPermission admin
 * @apiParam {String} access_token admin access token.
 * @apiParam sixCharId Bicycle's sixCharId.
 * @apiParam pickupLocation Bicycle's pickupLocation.
 * @apiParam status Bicycle's status.
 * @apiSuccess {Object} bicycle Bicycle's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Bicycle not found.
 * @apiError 401 admin access only.
 */
router.put('/:id',
  token({ required: true }),
  body({ sixCharId, pickupLocation, status, type }),
  update)

/**
 * @api {delete} /bicycles/:id Delete bicycle
 * @apiName DeleteBicycle
 * @apiGroup Bicycle
 * @apiPermission admin
 * @apiParam {String} access_token admin access token.
 * @apiSuccess (Success 204) 204 No Content.
 * @apiError 404 Bicycle not found.
 * @apiError 401 admin access only.
 */
router.delete('/:id',
  token({ required: true, roles: ['admin'] }),
  destroy)

export default router
