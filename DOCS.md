# baiku v0.0.0



- [Auth](#auth)
	- [Authenticate](#authenticate)
	
- [Bicycle](#bicycle)
	- [Create bicycle](#create-bicycle)
	- [Create pickup](#create-pickup)
	- [Delete bicycle](#delete-bicycle)
	- [Retrieve bicycle](#retrieve-bicycle)
	- [Retrieve bicycles](#retrieve-bicycles)
	- [Retrieve dropoff locations](#retrieve-dropoff-locations)
	- [Retrieve bicycle](#retrieve-bicycle)
	- [Show dropoff](#show-dropoff)
	- [Update bicycle](#update-bicycle)
	
- [DropOffLocation](#dropofflocation)
	- [Create drop off location](#create-drop-off-location)
	- [Delete drop off location](#delete-drop-off-location)
	- [Retrieve drop off location](#retrieve-drop-off-location)
	- [Retrieve drop off locations](#retrieve-drop-off-locations)
	- [Update drop off location](#update-drop-off-location)
	
- [PasswordReset](#passwordreset)
	- [Send email](#send-email)
	- [Submit password](#submit-password)
	- [Verify token](#verify-token)
	
- [PickupLocation](#pickuplocation)
	- [Create pickup location](#create-pickup-location)
	- [Delete pickup location](#delete-pickup-location)
	- [Retrieve pickup location](#retrieve-pickup-location)
	- [Retrieve pickup locations](#retrieve-pickup-locations)
	- [Update pickup location](#update-pickup-location)
	
- [Reservation](#reservation)
	- [Create reservation](#create-reservation)
	- [Delete reservation](#delete-reservation)
	- [Retrieve reservation](#retrieve-reservation)
	- [Retrieve reservations](#retrieve-reservations)
	- [Update reservation](#update-reservation)
	
- [User](#user)
	- [Create user](#create-user)
	- [Delete user](#delete-user)
	- [Retrieve current user](#retrieve-current-user)
	- [Retrieve user](#retrieve-user)
	- [Retrieve users](#retrieve-users)
	- [Update password](#update-password)
	- [Update user](#update-user)
	


# Auth

## Authenticate



	POST /auth

### Headers

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| Authorization			| String			|  <p>Basic authorization with email and password.</p>							|

### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>Master access_token.</p>							|

# Bicycle

## Create bicycle



	POST /bicycles


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>admin access token.</p>							|
| sixCharId			| 			|  <p>Bicycle's sixCharId.</p>							|
| pickupLocation			| 			|  <p>Bicycle's pickupLocation.</p>							|
| status			| 			|  <p>Bicycle's status.</p>							|

## Create pickup



	POST /bicycles/:sixCharId/pickup


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>admin access token.</p>							|
| sixCharId			| 			|  <p>Bicycle's sixCharId.</p>							|

## Delete bicycle



	DELETE /bicycles/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>admin access token.</p>							|

## Retrieve bicycle



	GET /bicycles/:id


## Retrieve bicycles



	GET /bicycles


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| q			| String			| **optional** <p>Query to search.</p>							|
| page			| Number			| **optional** <p>Page number.</p>							|
| limit			| Number			| **optional** <p>Amount of returned items.</p>							|
| sort			| String[]			| **optional** <p>Order of returned items.</p>							|
| fields			| String[]			| **optional** <p>Fields to be returned.</p>							|

## Retrieve dropoff locations



	GET /bicycles/:sixCharId/dropoff


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| q			| String			| **optional** <p>Query to search.</p>							|
| page			| Number			| **optional** <p>Page number.</p>							|
| limit			| Number			| **optional** <p>Amount of returned items.</p>							|
| sort			| String[]			| **optional** <p>Order of returned items.</p>							|
| fields			| String[]			| **optional** <p>Fields to be returned.</p>							|

## Retrieve bicycle



	GET /bicycles/pickup-location/:locationId


## Show dropoff



	POST /bicycles/:sixCharId/dropoff


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>admin access token.</p>							|
| sixCharId			| 			|  <p>Bicycle's sixCharId.</p>							|

## Update bicycle



	PUT /bicycles/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>admin access token.</p>							|
| sixCharId			| 			|  <p>Bicycle's sixCharId.</p>							|
| pickupLocation			| 			|  <p>Bicycle's pickupLocation.</p>							|
| status			| 			|  <p>Bicycle's status.</p>							|

# DropOffLocation

## Create drop off location



	POST /drop-off-locations


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>user access token.</p>							|
| lat			| 			|  <p>Drop off location's lat.</p>							|
| long			| 			|  <p>Drop off location's long.</p>							|

## Delete drop off location



	DELETE /drop-off-locations/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>user access token.</p>							|

## Retrieve drop off location



	GET /drop-off-locations/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>user access token.</p>							|

## Retrieve drop off locations



	GET /drop-off-locations


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>user access token.</p>							|
| q			| String			| **optional** <p>Query to search.</p>							|
| page			| Number			| **optional** <p>Page number.</p>							|
| limit			| Number			| **optional** <p>Amount of returned items.</p>							|
| sort			| String[]			| **optional** <p>Order of returned items.</p>							|
| fields			| String[]			| **optional** <p>Fields to be returned.</p>							|

## Update drop off location



	PUT /drop-off-locations/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>user access token.</p>							|
| lat			| 			|  <p>Drop off location's lat.</p>							|
| long			| 			|  <p>Drop off location's long.</p>							|

# PasswordReset

## Send email



	POST /password-resets


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| email			| String			|  <p>Email address to receive the password reset token.</p>							|
| link			| String			|  <p>Link to redirect user.</p>							|

## Submit password



	PUT /password-resets/:token


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| password			| String			|  <p>User's new password.</p>							|

## Verify token



	GET /password-resets/:token


# PickupLocation

## Create pickup location



	POST /pickup-locations


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>user access token.</p>							|
| lat			| 			|  <p>Pickup location's lat.</p>							|
| long			| 			|  <p>Pickup location's long.</p>							|

## Delete pickup location



	DELETE /pickup-locations/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>user access token.</p>							|

## Retrieve pickup location



	GET /pickup-locations/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>user access token.</p>							|

## Retrieve pickup locations



	GET /pickup-locations


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>user access token.</p>							|
| q			| String			| **optional** <p>Query to search.</p>							|
| page			| Number			| **optional** <p>Page number.</p>							|
| limit			| Number			| **optional** <p>Amount of returned items.</p>							|
| sort			| String[]			| **optional** <p>Order of returned items.</p>							|
| fields			| String[]			| **optional** <p>Fields to be returned.</p>							|

## Update pickup location



	PUT /pickup-locations/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>user access token.</p>							|
| lat			| 			|  <p>Pickup location's lat.</p>							|
| long			| 			|  <p>Pickup location's long.</p>							|

# Reservation

## Create reservation



	POST /reservations


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>user access token.</p>							|
| pickupLocation			| 			|  <p>Reservation's pickupLocation.</p>							|
| status			| 			|  <p>Reservation's status.</p>							|

## Delete reservation



	DELETE /reservations/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>user access token.</p>							|

## Retrieve reservation



	GET /reservations/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>user access token.</p>							|

## Retrieve reservations



	GET /reservations


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>user access token.</p>							|
| q			| String			| **optional** <p>Query to search.</p>							|
| page			| Number			| **optional** <p>Page number.</p>							|
| limit			| Number			| **optional** <p>Amount of returned items.</p>							|
| sort			| String[]			| **optional** <p>Order of returned items.</p>							|
| fields			| String[]			| **optional** <p>Fields to be returned.</p>							|

## Update reservation



	PUT /reservations/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>user access token.</p>							|
| pickupLocation			| 			|  <p>Reservation's pickupLocation.</p>							|
| status			| 			|  <p>Reservation's status.</p>							|

# User

## Create user



	POST /users


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>Master access_token.</p>							|
| email			| String			|  <p>User's email.</p>							|
| password			| String			|  <p>User's password.</p>							|
| name			| String			| **optional** <p>User's name.</p>							|
| picture			| String			| **optional** <p>User's picture.</p>							|
| role			| String			| **optional** <p>User's role.</p>							|

## Delete user



	DELETE /users/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>User access_token.</p>							|

## Retrieve current user



	GET /users/me


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>User access_token.</p>							|

## Retrieve user



	GET /users/:id


## Retrieve users



	GET /users


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>User access_token.</p>							|
| q			| String			| **optional** <p>Query to search.</p>							|
| page			| Number			| **optional** <p>Page number.</p>							|
| limit			| Number			| **optional** <p>Amount of returned items.</p>							|
| sort			| String[]			| **optional** <p>Order of returned items.</p>							|
| fields			| String[]			| **optional** <p>Fields to be returned.</p>							|

## Update password



	PUT /users/:id/password

### Headers

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| Authorization			| String			|  <p>Basic authorization with email and password.</p>							|

### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| password			| String			|  <p>User's new password.</p>							|

## Update user



	PUT /users/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>User access_token.</p>							|
| name			| String			| **optional** <p>User's name.</p>							|
| picture			| String			| **optional** <p>User's picture.</p>							|


