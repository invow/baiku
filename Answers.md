# Baiku Ansuers

- What is a stack? Have you ever had reason to use one?

A stack is an ordered list or data structure that allows you to store and retrieve data. Sure, I use it every day to resolve software engineering problems/

- What is time complexity? How would you characterize in words the
  difference between O(n) and O(log n)?

The amount of time it takes to run an algorithm.

For the input of size n, an algorithm of O(n) will perform steps perportional to n, while another algorithm of O(log(n)) will perform steps roughly log(n).
Clearly log(n) is smaller than n hence algorithm of complexity O(log(n)) is better. Since it will be much faster.
[Source: Stackoverflow]

- With respect to functions, what is the difference between “call by
  reference” and “call by value”?

In Call by value, a copy of the variable is passed whereas in Call by reference, a variable itself is passed.

- What is a hash function? Why are Javascript objects sometimes called
  “hashes”?

Converts one or more input elements to a function to another element.

- What does it mean for a database to be normalized? Why might you
  want a database to be normalized?

It is a process that consists of designating and applying a series of rules to the relationships obtained after passing from the entity-relationship model to the relational model in order to minimize data redundancy, facilitating its subsequent management.

- What might you expect the Mongo query `db.users.find({ userName: /[A-Z]+/ })`
  to return?


`/[A-Z]+/` It's a regular expresion. I am not so sure. I think it matches exactly one character from a-z


- Create repository
- Send an email
