# 99s / COOP Web Application Engineer Design Challenge

The goal of this challenge is to show us how you think about software
design in a distributed client/server/product environment. There are
also have a few computer-sciency questions at the end. Feel free to
refer to online resources, but try to respond from your own experience
and expertise first.


## Design Exercise

You are tasked with designing the backend for _Baiku_, an urban
bicycle rental platform.

- Baiku has autonomous _pickup_ and _dropoff_ locations in cities across
  the world.
- Baiku offers two classes of bicycle: _touring_ and _sport_.
- Baiku bicycles are equipped with _GPS trackers_ that continually
  update the platform with their location.
- Users can access Baiku via mobile app and website.
- Both the mobile app and website let users identify a bicycle by
  either putting in a unique _6-character ID_, or by imaging a _QR code_
  sticker.
- Baiku bicycles are locked to their location until the system
  _approves a pickup_.
- At dropoff, a Baiku location will read the bicycle's NFC tag and
  automatically lock and report the bicycle as dropped off (no need
  for the user to do anything in their app).

For this exercise, consider the following user stories:

- As a customer, I want to _reserve a specific bicycle—either_
  touring or sport—at a specific location, up to 24 hours in advance.

- As a customer _standing in front of a pickup location_, I want to
  _reserve a specific bicycle right now_.
  
- As a customer _with a rented bicycle_, I want Baiku to 
_show me dropoff locations near me_.


### Object Model

First, please design an object relational model for the following
system entities:

- User
- Bicycle
- PickupLocation
- Reservation

Include properties, relationships, and cardinality (one-to-one,
many-to-one, and so on). You may define Typescript types, create
diagrams, or use whatever other tools you feel comfortable using to
communicate the design at a high level.

Are there any other objects that should be included to support the
user stories? Feel free to include them.

### HTTP Endpoints

Next, please design a set of HTTP endpoints that could support the
above user stories and show a set of sample calls for each user story.

For example, an endpoint to retrieve all bicycles at a location might
be:

```
GET /bicycles/pickup-location/:locationId
```

An endpoint to unlock and pickup a specific bicycle might be:

```
POST /bicycles/:sixCharId/pickup
{
  userCoordinates: { lat: xxx.xxx, lng: xxx.xxx }
}
```

You can assume the user is already authenticated when making these
calls, and that the user has enabled location services on their
mobile device. Also assume JSON payloads for requests and responses.

### API Considerations

For these endpoints, what kinds of responses should the API send
to clients? What kind of validation of these requests should the
backend be doing? In what ways might these requests fail?

### Technology Considerations

At a high level, what technologies would you use to implement this
system at an MVP level? You can describe languages, frameworks,
AWS or Azure offerings, etc, if you're familiar with them. What are
trade-offs one might consider when making these decisions?


## General / Computer Science / Javascript

Try to answer from your own knowlege first!

- What is a stack? Have you ever had reason to use one?
- What is time complexity? How would you characterize in words the
  difference between O(n) and O(log n)?
- With respect to functions, what is the difference between “call by
  reference” and “call by value”?
- What is a hash function? Why are Javascript objects sometimes called
  “hashes”?
- What does it mean for a database to be normalized? Why might you
  want a database to be normalized?
- What might you expect the Mongo query `db.users.find({ userName: /[A-Z]+/ })`
  to return?

